# 파이썬에서 동적으로 변수를 생성하려면 locals()함수나 globals()함수를 사용해야 합니다.
#
# locals()함수는 현재 local변수들을 딕셔너리형태로 return합니다.
#
# globals()함수는 현재 global 변수들을 딕셔너리 형태로 return합니다.

def localTest():
    localVar1 = "Hi"
    localVar2 = 200
    localVar3 = [40, 50, 60]

    print("globals() in function\n", globals(), "\n")
    # 결과 : {생략, 'globalVar1': 'Hello', 'globalVar2': 100, 'globalVar3': [10, 20, 30]}

    print("locals() in function\n", locals(), "\n")
    # 결과 : {'localVar1': 'Hi', 'localVar2': 200, 'localVar3': [40, 50, 60]} 


globalVar1 = "Hello"
globalVar2 = 100
globalVar3 = [10, 20, 30]

print("globals() in main\n", globals(), "\n")
# 결과 : {생략, 'globalVar1': 'Hello', 'globalVar2': 100, 'globalVar3': [10, 20, 30]}

print("locals()in main\n", locals(), "\n")
# 결과 : {생략, 'globalVar1': 'Hello', 'globalVar2': 100, 'globalVar3': [10, 20, 30]}

localTest()

# 2. 파이썬 동적으로 변수 생성하기 (for문으로 변수 생성하기)
# globals()함수는 현재 global 변수들을 딕셔너리 형태로 return하므로,
#
# 기존 딕셔너리와 같은 방법으로 itme을 추가할 수 있습니다.
globals()['test'] =100
print(globals())


# for 문으로 변수 생성.
for i in range(1,4):
    globals()['test'+str(i)] = i* 10

print(globals())
print(test1, test2, test3)