import logging

mylogger= logging.getLogger("my")
mylogger.setLevel(logging.INFO)
# mylogger.basicConfig(filename='./test.log', level=logging.DEBUG)

# 출력 포메팅 설정
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

# stream handler 설정
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
mylogger.addHandler(stream_handler)

# file handler 설정
file_handler = logging.FileHandler("./logging_module_test/test.log")
mylogger.addHandler(file_handler)

mylogger.debug('debug')
mylogger.info('info')
mylogger.error('error')
mylogger.critical('critical')


