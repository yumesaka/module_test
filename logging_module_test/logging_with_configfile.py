import logging
import logging.config
import json
import os


class LoggerAdapter(logging.LoggerAdapter):

    def __init__(self, prefix, logger):
        super(LoggerAdapter, self).__init__(logger, {})
        self.prefix = prefix

    def process(self, msg, kwargs):
        return '[%s] %s' % (self.prefix, msg), kwargs


if __name__ == '__main__':
    with open("logging/config/logging_config.json", "rt") as f:
        config = json.load(f)

    logging.config.dictConfig(config)

    logger = logging.getLogger()
    logger.info('test~!!!')

    # 특정 로거의 설정 동작
    my_module_logger = logging.getLogger("my_module")
    my_module_logger.info("my_module_test~!!")

    # 부가정보 출력하기
    # 로깅 호출에 전달된 매개변수외에도 컨텍스트 정보를 포함하도록 로깅 출력

    logger = logging.getLogger("")
    logger = LoggerAdapter("SAS", logger)
    logger.info('test~!!!!!!')

    # logging.LoggerAdapter 를 상속받아서 만든 어댑터를 통해서 SAS라는 문구를 메세지 앞에 고정 시킬 수 있게 됩니다.
    # process메소드에 prefix를 추가한것을 확인하세요.

