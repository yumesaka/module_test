from logging_module_test.logging_singleton import CustomLogger

# 최초 한번 실행될 경우에는 __call__의 'Generate new instance' 가 출력되고 CustomLogger의 __init__이 호출된다.
logger = CustomLogger.__call__()
logger = CustomLogger.__call__()
logger.info("start logging...")

# 첫번째를 제외한 그 이후에는 'return instance'가 출력되고, init은 호출되지 않는다. (SingletonType class가 객체를 두개 이상 생성하는 것을 제한)
logger = CustomLogger.__call__().get_logger()
logger = CustomLogger.__call__().get_logger()