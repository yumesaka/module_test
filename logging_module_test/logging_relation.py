#로거의 부모 자식 관계
import logging

# 루트 로거 생성
rootlogger = logging.getLogger()
rootlogger.setLevel(logging.INFO)
stream_handler = logging.StreamHandler()
rootlogger.addHandler(stream_handler)

# my 로거 생성
mylogger = logging.getLogger("my")
mylogger.setLevel(logging.INFO)
stream_handler2 = logging.StreamHandler()
mylogger.addHandler(stream_handler2)

# my 로거 생성
mylogger.info("message from mylogger")

# message from mylogger
# message from mylogger
# 와 같이 상위 로거로 전파된다

# propagate 를 0 으로 세팅하면 상위로 전파하지 않습니다.
mylogger.propagate = 0
mylogger.info("message from mylogger")