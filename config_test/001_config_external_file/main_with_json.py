import json

with open('config.json', 'r') as f:
    config = json.load(f)

secret_key = config['DEFAULT']['SECRET_KEY']
ci_hook_url = config['CI']['HOOK_URL']

print(secret_key)
print(ci_hook_url)