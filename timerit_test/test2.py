import timeit

# https://brownbears.tistory.com/456

def test():
    return "-".join(str(n) for n in range(100))

# t1 = timeit.timeit("test()", setup='from __main__ import test', number=10000)
# t1 = timeit.repeat('test()', setup='from __main__ import test',  number=10000, repeat=10)
# print(t1)

timer = timeit.Timer("test()", setup='from __main__ import test')
t2 = timer.timeit(number=10000)
print(t2)

