import traceback
from multipledispatch import dispatch
from collections import Iterable
import psycopg2
from psycopg2.extras import RealDictCursor
from postgres.utils.config import HOST, PORT, DBNAME, USER, PWD


class DBManager:
    def __init__(self):
        self.conn = self.__connect()

    def teardown(self):
        if self.conn:
            self.conn.close()
            self.conn = None

    def __connect(self):
        print("database conneting....")
        return psycopg2.connect(host=HOST, port=PORT, dbname=DBNAME, user=USER, password=PWD)

    def __read_query(self, query_filename):
        query = ''
        with open(query_filename, 'r') as f:
            query = f.read()

        return query

    def execute_select(self, query):
        if not self.conn:
            print("__execute : " + "not connected, reconnect")
            self.conn = self.__connect()

        # RealDictCursor를 이용하여 cursor를 생성하면 쿼리 결과는
        # json 형식으로 표시됨
        cur = self.conn.cursor(cursor_factory=RealDictCursor)

        try:
            cur.execute(query)
            result = cur.fetchall()
        except Exception as e:
            msg = traceback.format_exc()
            msg += '\n\n Query: \n' + query
            print(msg)
            cur.close()

            return None

        cur.close()
        return result

    @dispatch(str)
    def execute_commit(self, query):
        if not self.conn:
            print("__execute : " + "not connected, reconnect")
            self.conn = self.__connect()

        # RealDictCursor를 이용하여 cursor를 생성하면 쿼리 결과는
        # json 형식으로 표시됨
        cur = self.conn.cursor(cursor_factory=RealDictCursor)

        try:
            cur.execute(query)
        except Exception as e:
            msg = traceback.format_exc()
            msg += '\n\n Query: \n' + query
            print(msg)
            cur.close()
            self.conn.rollback()

        cur.close()
        self.conn.commit()

    @dispatch(str, Iterable)
    def execute_commit(self, query, data):
        if not self.conn:
            print("__execute : " + "not connected, reconnect")
            self.conn = self.__connect()

        # RealDictCursor를 이용하여 cursor를 생성하면 쿼리 결과는
        # json 형식으로 표시됨
        cur = self.conn.cursor(cursor_factory=RealDictCursor)

        try:
            cur.execute(query, data)
        except Exception as e:
            msg = traceback.format_exc()
            msg += '\n\n Query: \n' + query
            print(msg)
            cur.close()
            self.conn.rollback()

        cur.close()
        self.conn.commit()
