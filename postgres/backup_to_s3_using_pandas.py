# import boto3
import postgres.utils.DBManager as DBM
import postgres.s3_util as s3_util
import pandas as pd
import os
import timeit
import time

a = DBM.DBManager()
# query_version = "select version()"
#
# print(a.execute_select(query_version))

# 쿼리 모음

q_get_device_id = "select distinct device_id from tb_opr_ess_state_hist " \
                  "where to_timestamp(colec_dt, 'YYYYMMDDHH24MISS') " \
                  "between date(now()) - '91 day'::interval and date(now()) - '90 day'::interval"

sql = "select * from tb_opr_ess_state_hist " \
      "where to_timestamp(colec_dt, 'YYYYMMDDHH24MISS') " \
      "between date(now()) - '91 day'::interval and date(now()) - '90 day'::interval " \
      "and device_id = '{0}' " \
      "order by colec_dt;"

# df_res = a.execute_select(q_get_device_id)
# print(df_res[0]['device_id'])


now = time.localtime()
start_time = time.time()

df_res = pd.read_sql_query(q_get_device_id, a.conn)
# df_res.to_csv(r"temp.csv")


for item in df_res['device_id']:
    print('csv start time : ', start_time - time.time())
    print(item)
    query = sql.format(item)
    print(query)

    df_bakcup_1min = pd.read_sql_query(query, a.conn)
    os.makedirs('./{0}'.format('backup'), exist_ok=True)
    file_name = './{0}/{1}${2}${3}${4}${5}'.format('backup', item, now.tm_year, now.tm_mon, now.tm_mday,
                                                   'ess_state_1min_data.csv')
    df_bakcup_1min.to_csv(file_name)

    print('csv end time : ', start_time - time.time())
    s3_util.upload_csv_to_s3(file_name, now)
    print('update past time : ', start_time - time.time())
