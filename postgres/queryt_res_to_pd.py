import postgres.utils.DBManager as DBM
import pandas as pd

a = DBM.DBManager()

# 쿼리 모음

q_get_device_id = "select distinct device_id from tb_opr_ess_state_hist " \
                  "where to_timestamp(colec_dt, 'YYYYMMDDHH24MISS') " \
                  "between date(now()) - '91 day'::interval and date(now()) - '90 day'::interval"

df_res = a.execute_select(q_get_device_id)
df_res_1 = pd.DataFrame(df_res)