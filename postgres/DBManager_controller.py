import postgres.utils.DBManager as DB
import sys


a = DB.DBManager()
query_version = "select version();"
# query_device_info = "select * from device_info limit 5;"

print(a.execute_select(query_version))
# print(a.execute_select(query_device_info))


# test 용 쿼리 모음
query_create_test_table = "CREATE TABLE test (id serial PRIMARY KEY, num integer, data varchar);"
query_drop_test_table = "DROP TABLE IF EXISTS test;"
query_insert_test = "INSERT INTO test (num, data) VALUES (%s, %s)"
data_insert_test = (100, "abc'def")
query_insert_test_2 = "INSERT INTO test (num) VALUES (%s)"
data_insert_test_2_1 = (101,)
data_insert_test_2_2 = [102,]
data_insert_test_2_2 = [102,]

a.execute_commit(query_drop_test_table)
a.execute_commit(query_create_test_table)
a.execute_commit(query_insert_test, data_insert_test)
a.execute_commit(query_insert_test_2, data_insert_test_2_1)
a.execute_commit(query_insert_test_2, data_insert_test_2_2)


#connection 유지 하여 insert
query_insert_test_3 = "INSERT INTO test (num) VALUES (%(num)s);"
data_insert_test_3_1 = [
    {'num' : i } for i in range(1000,1005)
]
for d in data_insert_test_3_1:
    a.execute_commit(query_insert_test_3, d)




sys.exit()
select_query = "select * from tb_bas_user limit 1;"

print(a.execute(select_query))
print(a.execute(select_query))
print(a.execute(select_query))
print(a.execute(select_query))
