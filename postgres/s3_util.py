import boto3
import time

def upload_csv_to_s3(self, file_name, now_time):
    s3 = boto3.client('s3')
    with open(file_name, "rb") as f:
        s3.upload_fileobj(f, "qhome-test", "backupdata-in-db/{0}/1min/{1}/{2}/{3}/ess_state_1min_data.csv"
                          .format(item, now_time.tm_year, now_time.tm_mon, now_time.tm_mday))