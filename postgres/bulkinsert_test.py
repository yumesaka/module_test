import postgres.utils.DBManager as DB
import sys
import timeit


def normal_insert():
    a = DB.DBManager()
    # connection 유지 하여 insert
    query_insert_test_3 = "INSERT INTO test (num) VALUES (%(num)s);"
    data_insert_test_3_1 = [
        {'num': i} for i in range(1000, 2000)
    ]
    for d in data_insert_test_3_1:
        a.execute_commit(query_insert_test_3, d)

    a.teardown()

start_time = timeit.default_timer()  # 시작 시간 체크

normal_insert()

terminate_time = timeit.default_timer()  # 종료 시간 체크
print("%f초 걸렸습니다." % (terminate_time - start_time))


def fast_insert():
    a = DB.DBManager()
    query_test = "INSERT INTO test(num) select  unnest(ARRAY '%(nums)s');"
    data_insert_test_3_1 = [
        {'num': i} for i in range(1000, 2000)
    ]
    nums = [r['num'] for r in data_insert_test_3_1]

    a.execute_commit(query_test, nums)

    a.teardown()


start_time = timeit.default_timer()  # 시작 시간 체크

fast_insert()

terminate_time = timeit.default_timer()  # 종료 시간 체크
print("%f초 걸렸습니다." % (terminate_time - start_time))

# todo
# 코드 속도 측정하는 거 알아보기